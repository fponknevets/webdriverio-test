const Page = require('./page.js');

class LoginPage extends Page {

    get username() { return $('#username') }
    get password() { return $('#password') }
    get submitBtn() { return $('form button[type="submit"]') }
    get flash() { return $('#flash') }
    get headerLinks() { return $$('#header a') }

    open() {
        super.open('https://the-internet.herokuapp.com/login');
        return this;
    }

    populate(user, password){
        this.username.setValue(user);
        this.password.setValue(password);
        return this;
    }

    submit() {
        this.submitBtn.click()
        return this;
    }

    login(user = 'tomsmith', password = 'SuperSecretPassword!'){
        this.populate(user,password);
        this.submit();
        return this;
    }

}

module.exports = new LoginPage();