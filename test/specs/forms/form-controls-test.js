const assert = require('assert');
const expect = require('chai').expect;

describe('test form controls', () => {

    beforeEach( () => {
        browser.url('https://the-internet.herokuapp.com');
    });

    describe('test checkboxes', () => {
        it('can click a checkbox', () => {
            $('a=Checkboxes').click();
    
            const checkbox1 = $('[type="checkbox"]:nth-child(1)');
            expect(checkbox1.isSelected()).to.be.false;
    
            checkbox1.click();
            expect(checkbox1.isSelected()).to.be.true;
        });
    });

    describe('text dropdowns', () => {
        it('can select an item from a drop-down', () => {
            $('a=Dropdown').click();
            const dropdownControl = $('#dropdown');
    
            dropdownControl.selectByVisibleText('Option 2');
            expect(dropdownControl.getValue()).to.equal('2');
            console.log(dropdownControl.getValue());
        });
    
    
    });

    
    describe('can test number inputs', () => {
        it('can add values to number input', () => {
            $('a=Inputs').click();
            const numberInput = $('input');
    
            numberInput.addValue(2);
            numberInput.addValue(3);    
            expect(numberInput.getValue()).to.equal('23');
        });
    
        it('can set values of number input', () => {
            $('a=Inputs').click();
            const numberInput = $('input');
    
            numberInput.addValue(2);
            numberInput.addValue(3);    
            expect(numberInput.getValue()).to.equal('23');
    
            numberInput.setValue('34');
            expect(numberInput.getValue()).to.equal('34');
        });
    
        it('can clear value of number input', () => {
            $('a=Inputs').click();
            const numberInput = $('input');
    
            numberInput.setValue('34');
            expect(numberInput.getValue()).to.equal('34');
    
            numberInput.clearValue();
            expect(numberInput.getValue()).to.equal('');
        });
    });
});