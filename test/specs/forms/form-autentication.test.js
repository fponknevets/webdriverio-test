const expect = require('chai').expect;

describe('test form authentication, +ve and -ve tests', () => {
    
    beforeEach( () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Form Authentication').click();
    });
    
    it('should deny access with wrong username and password', () => {
        const user = $('#username');
        const pass = $('#password');
        const login = $('.fa-sign-in');
        user.addValue('foo');
        pass.addValue('bar!');
        login.click();
        const loginMessageText = $('#flash').getText();

        expect(loginMessageText).to.equal('Your username is invalid!\n×');
    })

    it('should login with valid username and password', () => {
        const user = $('#username');
        const pass = $('#password');
        const login = $('.fa-sign-in');
        user.addValue('tomsmith');
        pass.addValue('SuperSecretPassword!');
        login.click();
        const loginMessageText = $('#flash').getText();

        expect(loginMessageText).to.equal('You logged into a secure area!\n×');
    });
    
    // it('should not login with empty username and password', () => {

    // });
});