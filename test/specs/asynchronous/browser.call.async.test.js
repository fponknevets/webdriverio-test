const expect = require('chai').expect;

const dummyText = 'Some output from our fake API call.'

describe('deomstrating async calls', () => {

    it('does NOT use async bowser.call - creates an AssertionError', () => {
        const output = fakeAxios();
        // will fail because the above has not finished executing
        try {
            // console.log(output);
            expect(output).to.equal(dummyText)
        }
        catch(error) {
            // console.log('Console logging: ' + error.message);
            expect(error.message).to.equal(`expected {} to equal \'${dummyText}\'`);
        }
    });

    it('does use async bowser.call - expect this to pass', () => {
        const output = browser.call( () => {
            return fakeAxios();
        })
        // should pass because the code below when not execute until browser.call has completed
        // console.log(output);
        expect(output).to.equal(dummyText)
    });


});

// helper functions outside the describe
function fakeAxios() {
    return new Promise((resolve, reject) => {
        setTimeout( () => {
            resolve(dummyText);
        }, 3000 )
    });
}