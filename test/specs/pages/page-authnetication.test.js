const LoginPage = require('../../../framework/pages/login.page.js');

describe('login form', () => {
    it('should deny access with wrong username and password', () => {
        LoginPage.open().login('foo', 'bar');
        expect(LoginPage.flash).toHaveText('Your username is invalid!\n×')
    })

    it('should login with valid username and password', () => {
        LoginPage.open().login();
        expect(LoginPage.flash).toHaveText('You logged into a secure area!\n×')
    })
})