const assert = require('assert');
const expect = require('chai').expect;

describe('SELECTORS', () => {

    it('can find multiple elements with double dollar sign', () => {
        browser.url('https://the-internet.herokuapp.com');
        const links = $$('a');
        expect(links.length).to.equal(46);
    });
});

