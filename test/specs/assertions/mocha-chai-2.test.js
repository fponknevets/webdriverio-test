const assert = require('assert');

describe('webdriver.io page 2', () => {
    it('visible title on page should be correct', () => {
        browser.url('https://webdriver.io');
        const title = $('.projectTitle').getText();
        assert.equal(title, 'WEBDRIVER I/O');
    });
});