// mocha test framework
// chai an assertion library

const assert = require('assert');
const expect = require('chai').expect;

// import { equal } from 'assert';
// import { expect } from 'chai'

// notice the describe does not use fat arrow functions, so that the keyword 'this' works correctly
describe('webdriver.io page', function() {

    // set the timeout for this describe function
    this.timeout(10000);    // can also be set in the wdio.conf.js with mochaOpts: {}
    // will try a test THREE times
    this.retries(2);        // can also be set in the wdio.conf.js with mochaOpts: {}

    before( () => {
        //console.log('****** before() ******')
    })
    beforeEach( () => {
        //console.log('****** beforeEach() ******')
        browser.url('https://webdriver.io');
    });

    afterEach( () => {
        //console.log('****** afterEach() ******')
    })
    after( () => {
        //console.log('****** after() ******')
    });
   
    it('document title should be correct', () => {
        const title = browser.getTitle()
        // expect(browser).toHaveTitle('WebdriverIO · Next-gen browser and mobile automation test framework for Node.js');
        assert.equal(title, 'WebdriverIO · Next-gen browser and mobile automation test framework for Node.js');
    });

    it('page title should be displayed', () => {
        // dollar sign ($) calls findElement
        const title = $('.projectTitle');
        expect(title.isDisplayed(), 'page title is not displayed').to.equal(true);
    });

    it('page title should be correct', () => {
        const title = $('.projectTitle').getText();
        // assert.equal(title, 'WEBDRIVER I/O');
        expect(title).to.equal('WEBDRIVER I/O');
        // expect(title).to.not.equal('WEBDRIVER I/O');
    });

    it('can check the contents of an array', () => {
        expect([1,3,5]).to.include(5);
    });

    it('identify all the links with class button on the page', () => {
        console.log($$('a.button').length);
        $$('a.button').forEach(a => console.log(a.getAttribute('href')));
        expect($$('a.button')[3].getText()).to.equal('SUPPORT');
    });

});