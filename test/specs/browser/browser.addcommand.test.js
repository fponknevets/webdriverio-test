const expect = require('chai').expect;

before( () => {
    browser.addCommand("getUrlAndTitle", function (customVar) {
        return {
            url: this.getUrl(),
            title: this.getTitle(),
            customVar: customVar
        };
    });
});

describe('adding custom functionality to the browser object', () => {

    it('should use my custom command', () => {
        browser.url('http://www.github.com')
        var response = browser.getUrlAndTitle('foobar')
        expect(response.url).to.equal('https://github.com/');
        expect(response.title).to.equal('The world’s leading software development platform · GitHub');
        expect(response.customVar).to.equal('foobar');
    });

});