const assert = require('assert');
const expect = require('chai').expect;

describe('runing client side scripts', () => {

    beforeEach( () => {
        browser.url('https://the-internet.herokuapp.com');
    });

    it('passing values to the client side script by using browser.execute', () => {
        const elemSelector = '.heading';
        const elem = $(elemSelector);
        const elemText = 'Tada';

        browser.execute( (foo, bar) => {
            // runs in the browser and not on the server
            // so does not have access to the 'browser' object
            document.querySelector(foo).style.color = 'blue';
            document.querySelector(foo).textContent = bar;
        }, elemSelector, elemText); // elemSelector and elemText get passed in to browser.execute as positional arguments

        console.log(elem.getCSSProperty('color'));
        expect(elem.getText()).to.equal(elemText);
    });

    it('getting values out from the client side script by using browser.execute', () => {
        const elemSelector = '.heading';
        const elem = $(elemSelector);
        const elemText = 'Tada';

        const titleColor = browser.execute( (foo) => {
            // runs in the browser and not on the server
            // so does not have access to the 'browser' object
            document.querySelector(foo).style.color = 'blue';
            return document.querySelector(foo).style.color;

        }, elemSelector); // elemSelector gets passed in to browser.execute as a positional argument

        expect(titleColor).to.equal('blue');
    });


});