# Notes from the [WebdriverIO Tutorial](https://www.youtube.com/playlist?list=PL8HowI-L-3_9Ep7lxVrRDF-az5ku4sur_) by Will Brock
These are the notes I took as I followed the above Youtube webdriver.io tutorials
<p>&nbsp;</p>

## SETUP WEBDRIVERIO
These setup instrctions are there for me to replicate my setup if I need to using Linux (Ubuntu). They work for me in that environment but YMMV.
<p>&nbsp;</p>

### Install Node
* Install [Node Version Manager (nvm)](https://github.com/nvm-sh/nvm)
 ```
      curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
 ```

* Install node
 ```
      nvm install --lts
 ```

* Check the installed version of node
```
      node -v
```
<p>&nbsp;</p>

### Install Chromedriver

* Instal [Chromedriver through npm](https://www.npmjs.com/package/chromedriver)
```
      npm install chromedriver
```
* Check the installed version of chromedriver
```
      chromedriver -v
```
* Go to [chromium.org](https://chromedriver.chromium.org/downloads) to check the installed version of chrome driver supports the installed version of Chrome.
<p>&nbsp;</p>

### Install and Configure WebdriverIO

#### Installation

* Create a directory for webdriverio tests
```
      mkdir wdio-tests
```

* Initialise the directory for node
```
      cd wdio-tests
      npm init -y
```

* Install webdriverio
```
      npm install -D webdriverio
```
-D or --save-dev puts the package in the `devDependencies` sestion of the package.json

* Install the webdriverio command line interface tool
```
      npm install -D wdio-cli
```

#### Configuration
* Run the configuration wizard by typing ...
```
      ./node_modules/.bin/wdio
```

Settings will be stored in the `wdio.conf.js` file.

* Create a logs directory ...
```
      mkdir logs
```
* Add logs directory to the `wdio.conf.js` file ...
```javascript
      logLevel: 'info',
      outputDir: './logs',
```
* Add a diectory for tests ...

    It should match the directory specified in the wdio setup wizard (above) ...
```
      mkdir test/specs
```

 ... but you can change the `wdio.conf.js` file if you want to change the location
 ```javascript
      specs: [
        './test/specs/**/*.js'
      ],

 ```
<p>&nbsp;</p>


### Install Babel
This is a dodgy area - you need Babel to be able to write in ES6 and use things like imports. But I have not been able to get it to work and so I do not use ES6 in this repo at the moment.
```
npm install --save-dev @babel/core @babel/cli @babel/preset-env @babel/register
```
Create a `babel.config.js` file
```
touch babel.config.js
```
```javascript
module.exports = {
    presets: [
        ['@babel/preset-env', {
            targets: {
                node: "current"
            }
        }]
    ]
}
```


### Install and Configure Chai
```
npm i -D chai
```


 <p>&nbsp;</p>

## RUNNING TESTS
<p>&nbsp;</p>


### Start Chromedriver
In my experience this does not appear to be necessary, at least I never do this step and mocha just starts the chrome driver automatically.
```
    chromedriver --port=4444 --url-base=/wd/hub --verbose
```
<p>&nbsp;</p>

### Create a simple test and save it in the location specified in the `wdio.conf.js` file
```javascript
        // in file ./test/specs/getting-started/browser.title.test.js
        describe('webdriver.io page', () => {
            it('should have the right title', () => {
                browser.url('https://webdriver.io')
                const title = browser.getTitle()
                expect(browser).toHaveTitle('WebdriverIO · Next-gen browser and mobile automation test framework for Node.js');
            })
        })
```
### Run ALL tests:

```
 ./node_modules/.bin/wdio wdio.conf.js
 ```
 from:
 ```
 ~/repositories/wdio/webdriverio-test/
 ```
### Run specific spec file(s) using the `--spec` parameter:

```
 ./node_modules/.bin/wdio wdio.conf.js --spec [text to partial match spec file name(s)]
 ```
<p>&nbsp;</p>

## MORE CONCEPTS COVERED

|Concept                      |Coverage in the Tutorial                                         |Coverage in this project's files                                                                                          |Official Docs                       |
|-                            |-                                                                |-                                                                                                                         |-                          |
|before() & beforeEach()      |[06 Mocha](https://youtu.be/fmLbE-NcAQY?t=202)                   |[mocha-chai-1.test.js](test/specs/assertions/mocha-chai-1.test.js)                                                                   |                           |
|timeout                      |[06 Mocha](https://youtu.be/fmLbE-NcAQY?t=427)                   |[wdio.conf](wdio.conf.js) look for timout in mochaOpts<br>[moch-chai-1.test.js](test/specs/assertions/mocha-chai-1.test.js) line 14  |[mochajs.org](https://mochajs.org/#timeouts)|
|retries                      |[06 Mocha](https://youtu.be/fmLbE-NcAQY?t=591)                   |                                                                                                                          |[mochajs.org](https://mochajs.org/#retry-tests)|
|expect (Chai)                |[07 Assertions with Chai](https://youtu.be/v8erzMZbmew?t=22)     |                                                                                                                          |[chaijs.com](https://www.chaijs.com/api/bdd/)|
|$ and $$                     |[08 Find elements](https://youtu.be/z3_PZVgk32Y)                 |                                                                                                                          |[Selectors](https://webdriver.io/docs/selectors.html)<br>[findElement](https://webdriver.io/docs/api/webdriver.html#findelement)<br>[$](https://webdriver.io/docs/api/element/$.html)<br>[findElements](https://webdriver.io/docs/api/webdriver.html#findelements)<br>[$$](https://webdriver.io/docs/api/element/$$.html)|
|browser object               |[14 Global browser object](https://youtu.be/2dTFzu-S4Ag)         |                                                                                                                          |[The Browser Onbject - Webdriver.io](https://webdriver.io/docs/browserobject.html)|
|Client-side Javascript       |[16 Browser execute](https://youtu.be/K1ttmGmY2uE)               |[browser.execute.test.js](test/specs/client-side/browser.execute.test.js)                                                 |[Execute - webdriver.io](https://webdriver.io/docs/api/browser/execute.html)|
|asynchronous actions         |[17 Browser call](https://youtu.be/8U89zUwKkQE)                  |[browser.call.async.tests.js](test/specs/asynchronous/browser.call.async.test.js)                                         |[Call Webdriver.io](https://webdriver.io/docs/api/browser/call.html)|
|browser.addCommand           |[18 Browser addCommand](https://youtu.be/hy9WNW7G8j0)            |[browser.addcommand.test.js](test/specs/browser/browser.addcommand.test.js)                                               |[Custom Commands](https://webdriver.io/docs/customcommands.html)       |
|Page Objects                 |[21 Page object patter](https://youtu.be/uISxsZdtvg0)            |[login.page.js](framework/pages/login.page.js) and [page.authentication.test.js](test/specs/pages/page-authnetication.test.js)|[Page Object Pattern](https://webdriver.io/docs/pageobjects.html)       |

